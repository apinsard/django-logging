# -*- coding: utf-8 -*-
import re

from django.conf import settings
from django.contrib.auth import SESSION_KEY as AUTH_SESSION_KEY

from .models import HttpRequestLog, HttpResponseLog
from . import local

__all__ = [
    'DjangocopMiddleware',
]


class DjangocopMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        exempt_patterns = getattr(settings, 'DJANGOCOP_EXEMPT_PATTERNS', [])[:]
        exempt_patterns.extend([
            r'^{}'.format(re.escape(settings.STATIC_URL)),
            r'^{}'.format(re.escape(settings.MEDIA_URL)),
        ])
        for pattern in settings.DJANGOCOP_EXEMPT_PATTERNS:
            if re.search(pattern, request.path):
                return self.get_response(request)

        if hasattr(request, 'session'):
            user_id = request.session.get(AUTH_SESSION_KEY) or None
            sessions = dict(request.session)
        else:
            user_id = None
            sessions = None

        headers = {}
        for k, v in request.META.items():
            reg = re.compile(r'^(?:HTTP_)?(.*)$')
            if k.startswith('HTTP_'):
                name = reg.sub(
                    lambda m: m.group(1).replace('_', '-').title(), k)
                headers[name] = v
        headers.pop('Cookies', None)
        referer = headers.pop('Referer', '')
        user_agent = headers.pop('User-Agent', '')

        try:
            body = request.body.decode('utf-8')
        except UnicodeDecodeError:
            body = ''

        request_log = HttpRequestLog.objects.create(
            body=body[:65536],
            ip=self.get_remote_addr(request),
            method=request.method,
            path=request.path,
            scheme=request.scheme,
            content_type=request.content_type,
            length=int(request.META.get('CONTENT_LENGTH', '0') or '0'),
            query_string=request.GET.urlencode(),
            post_data=request.POST.urlencode(),
            cookies=request.COOKIES,
            headers=headers,
            referer=referer,
            user_agent=user_agent,
            sessions=sessions,
            user_id=user_id,
        )

        local.request_id = request.djangocop_request_log_id = request_log.pk
        response = self.get_response(request)

        headers = dict(response._headers.values())
        content_type = headers.get('Content-Type', '').split(';')[0]
        length = len(getattr(response, 'content', b''))

        HttpResponseLog.objects.create(
            parent_id=request.djangocop_request_log_id,
            status=response.status_code,
            content_type=content_type,
            length=length,
            headers=headers,
        )

        return response

    def get_remote_addr(self, request):
        reg = re.compile(
            r'^(\d{1,3}(\.\d{1,3}){3}|[0-9a-f]{,4}(:[0-9a-f]{,4}){1,7})$')
        remote_addr = request.META.get('REMOTE_ADDR')
        if not remote_addr or not reg.match(remote_addr):
            remote_addr = request.META.get('X_REAL_IP')
        if not remote_addr or not reg.match(remote_addr):
            remote_addr = request.META.get('HTTP_X_FORWARDED_FOR')
        if remote_addr and ',' in remote_addr:
            remote_addr = remote_addr.split(',')[0]
        return remote_addr or '127.0.0.1'
