# -*- coding: utf-8 -*-
from django.template import Library

__all__ = [
    'url_replace',
]

register = Library()


@register.simple_tag(takes_context=True)
def url_replace(context, field, value):
    request = context['request']
    querystring = request.GET.copy()
    querystring[field] = value
    return "?{}".format(querystring.urlencode())
