# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic import ListView, DetailView

from .models import Log, HttpRequestLog

__all__ = [
    'HttpRequestLogListView', 'LogDetailView',
]


class IsSuperuserMixin(UserPassesTestMixin):

    def test_func(self):
        return self.request.user.is_superuser


class HttpRequestLogListView(IsSuperuserMixin, ListView):

    model = HttpRequestLog
    paginate_by = 50
    template_name = 'djangocop/log_list.html'
    ordering = ['-created_on']


class LogDetailView(IsSuperuserMixin, DetailView):

    model = Log

    def get_object(self):
        obj = super().get_object()
        return obj.get_actual_object()

    def get_template_names(self):
        return [
            'djangocop/{}_detail.html'.format(self.object._meta.model_name),
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subsequent_logs'] = self.object.subsequent_logs.all()
        return context
