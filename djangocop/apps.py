# -*- coding: utf-8 -*-
from django.apps import AppConfig

__all__ = [
    'DjangocopConfig',
]


class DjangocopConfig(AppConfig):
    name = 'djangocop'
    verbose_name = "DjangoCop"
