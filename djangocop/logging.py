# -*- coding: utf-8 -*-
from datetime import datetime
import logging

from django.utils import timezone

from . import local

__all__ = [
    'DjangocopHandler',
]


class DjangocopHandler(logging.Handler):

    def emit(self, record):
        from .models import PythonLog, PythonExceptionLog

        exc_info = record.exc_info
        if exc_info:
            log = PythonExceptionLog()
            log.exception_type = str(exc_info[0])
            log.exception_message = str(exc_info[1])
        else:
            log = PythonLog()

        tz = timezone.get_current_timezone()
        log.created_on = datetime.fromtimestamp(record.created, tz=tz)
        log.level_id = record.levelno // 10
        log.parent_id = getattr(local, 'request_id', None)
        log.body = record.getMessage()
        log.logger_name = record.name
        log.file_name = record.pathname
        log.line = record.lineno
        log.message_template = record.msg
        log.function_name = record.funcName
        log.stack_info = record.stack_info or ''

        log.save()
