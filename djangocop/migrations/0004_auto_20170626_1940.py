# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-26 19:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('djangocop', '0003_auto_20170625_1734'),
    ]

    operations = [
        migrations.CreateModel(
            name='PythonLog',
            fields=[
                ('log_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='djangocop.Log')),
                ('logger_name', models.CharField(max_length=255)),
                ('file_name', models.CharField(max_length=255)),
                ('line', models.PositiveIntegerField()),
                ('message_template', models.TextField()),
                ('function_name', models.CharField(max_length=255)),
                ('stack_info', models.TextField(blank=True)),
            ],
            bases=('djangocop.log',),
        ),
        migrations.AlterField(
            model_name='log',
            name='created_on',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='loglevel',
            name='severity',
            field=models.PositiveSmallIntegerField(unique=True),
        ),
        migrations.CreateModel(
            name='PythonExceptionLog',
            fields=[
                ('pythonlog_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='djangocop.PythonLog')),
                ('exception_type', models.CharField(max_length=255)),
                ('exception_message', models.TextField(blank=True)),
            ],
            bases=('djangocop.pythonlog',),
        ),
    ]
