# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

__all__ = [
    'urlpatterns',
]


urlpatterns = [
    url(r'^$', views.HttpRequestLogListView.as_view(), name='log_list'),
    url(r'^log/(?P<pk>[0-9]+)/$', views.LogDetailView.as_view(),
        name='log_detail'),
]
