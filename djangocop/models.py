# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.postgres.fields import HStoreField, JSONField
from django.db import models
from django.http import QueryDict
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property

__all__ = [
    'LogLevel', 'Log', 'HttpRequestLog', 'HttpResponseLog', 'PythonLog',
    'PythonExceptionLog',
]


class LogLevel(models.Model):

    DEBUG = 1
    INFO = 2
    WARNING = 3
    ERROR = 4
    FATAL = 5

    slug = models.SlugField(max_length=50, unique=True)
    name = models.CharField(max_length=50, unique=True)
    severity = models.PositiveSmallIntegerField(unique=True)
    color = models.CharField(max_length=6)

    def __str__(self):
        return "{} [{}]".format(self.name, self.severity)


class Log(models.Model):

    created_on = models.DateTimeField(default=timezone.now)
    level = models.ForeignKey(LogLevel, models.PROTECT, default=LogLevel.INFO)
    parent = models.ForeignKey(
        'self', models.CASCADE, blank=True, null=True,
        related_name='subsequent_logs')
    body = models.TextField(blank=True)
    metadata = JSONField(blank=True, null=True)

    def __str__(self):
        return "Log #{}".format(self.pk)

    def get_absolute_url(self):
        return reverse('djangocop:log_detail', kwargs={'pk': self.pk})

    def get_actual_object(self):
        try:
            return self.httprequestlog
        except Log.DoesNotExist:
            pass
        try:
            return self.httpresponselog
        except Log.DoesNotExist:
            pass
        try:
            return self.pythonlog
        except Log.DoesNotExist:
            pass
        return self


class HttpRequestLog(Log):

    ip = models.GenericIPAddressField()
    method = models.CharField(max_length=50)
    path = models.TextField()
    scheme = models.CharField(max_length=10)
    content_type = models.TextField(blank=True)
    length = models.BigIntegerField(default=0)
    query_string = models.TextField(blank=True)
    post_data = models.TextField(blank=True)
    cookies = HStoreField(blank=True)
    headers = HStoreField()
    referer = models.TextField(blank=True)
    user_agent = models.TextField(blank=True)
    route = models.CharField(max_length=255, blank=True)
    sessions = JSONField(blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.SET_NULL, blank=True, null=True)

    @cached_property
    def response(self):
        log = self.subsequent_logs.filter(
            httpresponselog__isnull=False).first()
        if log:
            return log.httpresponselog

    @property
    def full_url(self):
        query_string = self.query_string
        if query_string:
            query_string = '?' + query_string
        return '{scheme}://{host}{path}{query}'.format(
            scheme=self.scheme, host=self.host, path=self.path,
            query=query_string)

    @property
    def host(self):
        return self.headers.get('Host') or self.headers.get('HTTP_HOST')

    @cached_property
    def post_data_dict(self):
        return QueryDict(self.post_data)


class HttpResponseLog(Log):

    status = models.CharField(max_length=3)
    content_type = models.TextField()
    length = models.BigIntegerField(default=0)
    headers = HStoreField()


class PythonLog(Log):

    logger_name = models.CharField(max_length=255)
    file_name = models.CharField(max_length=255)
    line = models.PositiveIntegerField()
    message_template = models.TextField()
    function_name = models.CharField(max_length=255)
    stack_info = models.TextField(blank=True)


class PythonExceptionLog(PythonLog):

    exception_type = models.CharField(max_length=255)
    exception_message = models.TextField(blank=True)
